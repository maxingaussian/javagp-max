package com.max.gp.opt;

import java.util.Random;
import java.util.function.Function;

import org.jblas.DoubleMatrix;

public abstract class Optimizer {

	public static final double abs_grad_stop = 1e-2, diverge_break = 1000;
	
	int paramsSize, total, sample, iter, countReverse;
	double minObj, absGrad, obj, lastObj;
	boolean BREAK;
	DoubleMatrix best, params, objGrad;
	Function<DoubleMatrix, Void> setParamsFunc;
	Function<Void, Double> objFunc;
	Function<Void, DoubleMatrix> objFuncGrad;
	
	public void init(
			int paramsSize_,
			Function<DoubleMatrix, Void> setParamsFunc_, 
			Function<Void, Double> objFunc_, 
			Function<Void, DoubleMatrix> objFuncGrad_
			){
		paramsSize = paramsSize_;
		setParamsFunc=setParamsFunc_;
		objFunc=objFunc_;
		objFuncGrad=objFuncGrad_;
	}
	
	public abstract void everySampleDo();
	
	public abstract void everyIterationDo();
	
	public void run(int samples, int iterations, boolean message) {
		total = 0;
		sample = 1;
		minObj = 1e20; 
		do{
			params = DoubleMatrix.rand(paramsSize).add(1E-2);
			everySampleDo();
			BREAK = false;
			iter = 1;
			countReverse = 0;
			do{
				setParamsFunc.apply(params);
				objGrad = objFuncGrad.apply(null);
				absGrad = objGrad.norm1();
				obj = objFunc.apply(null);
				if(obj<minObj&&(lastObj-obj)/Math.abs(lastObj)<0.3){
					minObj=obj;
					best=params.dup();
				}
				if(obj*0.9>lastObj){
					countReverse++;
				}
				else{
					countReverse=0;
				}
				if(countReverse==3)break;
				everyIterationDo();
				if(message){
					System.out.println("-------------------------------------------------------");
					System.out.println("\tSample "+sample+" Iteration "+iter);
					System.out.println("\tObj = "+obj);
					System.out.println("\tObj Abs Grad = "+absGrad);
					System.out.println("-------------------------------------------------------");
				}
				if(BREAK)break;
			}while(iter++<iterations&&absGrad/paramsSize>abs_grad_stop);
			total+=iter-1;
		}while(sample++<samples);
		setParamsFunc.apply(best);
		System.out.println("=========================================================");
		System.out.println("\tMin Obj = "+minObj);
		System.out.println("\tTotal Samples = "+samples);
		System.out.println("\tTotal Iterations = "+total);
		System.out.println("=========================================================");
	}
}
