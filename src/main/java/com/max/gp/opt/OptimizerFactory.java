package com.max.gp.opt;

import com.max.gp.opt.CG.BetaType;
import com.max.gp.opt.Rprop.Model;

public class OptimizerFactory {

	public static Optimizer RpropPlus(){
		return new Rprop(Model.Rprop_plus);
	}
	
	public static Optimizer RpropMinus(){
		return new Rprop(Model.Rprop_minus);
	}
	
	public static Optimizer iRpropPlus(){
		return new Rprop(Model.iRprop_plus);
	}
	
	public static Optimizer iRpropMinus(){
		return new Rprop(Model.iRprop_minus);
	}
	
	public static Optimizer CGFR(){
		return new CG(BetaType.FLETCHER_REEVES);
	}
	
	public static Optimizer CGPR(){
		return new CG(BetaType.POLAK_RIBIERE);
	}
	
}
