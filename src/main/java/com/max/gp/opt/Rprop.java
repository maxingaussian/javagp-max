package com.max.gp.opt;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

public class Rprop extends Optimizer{
	
	public static enum Model {
        /** Improving the Rprop Learning Algorithm (2000) by Christian Igel , Michael Hüsken **/
		Rprop_plus,
		Rprop_minus,
		iRprop_plus,
		iRprop_minus
    }
	
	private static final double delta_init = 0.01, delta_min = 1e-6, delta_max = 50, eta_minus = 0.5, eta_plus = 1.2;
	
	Model model;
	DoubleMatrix delta, delta_params, lastObjGrad;
	
	public Rprop(Model model_) {
		model = model_;
	}
	
	@Override
	public void everySampleDo() {
		lastObj = 1e20;
		lastObjGrad = DoubleMatrix.zeros(paramsSize);
		delta_params = DoubleMatrix.zeros(paramsSize);
		delta = DoubleMatrix.ones(paramsSize).mul(delta_init);
	}

	@Override
	public void everyIterationDo() {
		for(int i=0;i<paramsSize;i++){
			if(lastObjGrad.get(i)*objGrad.get(i)>0){
				delta.put(i, Math.min(delta.get(i)*eta_plus, delta_max));
				switch (model) {
				case Rprop_plus:
				case iRprop_plus:
					delta_params.put(i, -Math.signum(objGrad.get(i))*delta.get(i));
					params.put(i, params.get(i)+delta_params.get(i));
					break;
				default:
					break;
				}
			}
			else if(lastObjGrad.get(i)*objGrad.get(i)<0){
				delta.put(i, Math.max(delta.get(i)*eta_minus, delta_min));
				switch (model) {
				case Rprop_plus:
					params.put(i, params.get(i)-delta_params.get(i));
					objGrad.put(i, 0);
					break;
				case iRprop_plus:
					if(obj>lastObj){
						params.put(i, params.get(i)-delta_params.get(i));
					}
					objGrad.put(i, 0);
					break;
				case iRprop_minus:
					objGrad.put(i, 0);
					break;
				default:
					break;
				}
			}
			else{
				switch (model) {
				case Rprop_plus:
				case iRprop_plus:
					delta_params.put(i, -Math.signum(objGrad.get(i))*delta.get(i));
					params.put(i, params.get(i)+delta_params.get(i));
					break;
				default:
					break;
				}
			}
			switch (model) {
			case Rprop_minus:
			case iRprop_minus:
				params.put(i, params.get(i)-Math.signum(objGrad.get(i))*delta.get(i));
				break;
			default:
				break;
			}
			lastObjGrad.put(i,objGrad.get(i));
		}
		lastObj = obj;
	}

}
