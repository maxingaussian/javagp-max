package com.max.gp.opt;

import org.jblas.DoubleMatrix;

public class CG extends Optimizer {

	public static enum BetaType {
        /** Fletcher-Reeves formula. */
        FLETCHER_REEVES,
        /** Polak-Ribière formula. */
        POLAK_RIBIERE
    }
	
	public static final int searchIterations = 100, restartIterations = 50;
	public static final double sigma_init = 0.1, search_err = 1e-5;
	
	BetaType betaType;
	int countRestart, searchIter;
	boolean begin;
	double alpha, sigma, beta, delta_init, delta_new, delta_old, delta_d;
	DoubleMatrix r_old, r_new, d, tmpObjGrad;
	
	public CG(BetaType betaType_) {
		betaType=betaType_;
	}
	
	@Override
	public void everySampleDo() {
		countRestart=0;
		r_new = objGrad.neg();
		d = r_new;
		delta_new = r_new.dot(r_new);
		delta_init = delta_new;
	}

	@Override
	public void everyIterationDo() {
		searchIter=0;
		delta_d=d.dot(d);
		do{
			sigma=(searchIter==0)?sigma_init:-alpha;
			setParamsFunc.apply(params.add(d.mul(sigma)));
			tmpObjGrad=objFuncGrad.apply(null);
			alpha=objGrad.dot(d);
			alpha=sigma_init*alpha/(alpha-tmpObjGrad.dot(d));
			params=params.add(d.mul(alpha));
			setParamsFunc.apply(params);
			objGrad=objFuncGrad.apply(null);
			searchIter++;
		}while(searchIter<searchIterations&&alpha*alpha*delta_d>search_err);
		r_old = r_new;
		r_new = objGrad.neg();
		delta_old = delta_new;
		delta_new = r_new.dot(r_new);
		if(betaType==BetaType.FLETCHER_REEVES){
			beta = delta_new/delta_old;
		}
		else if(betaType==BetaType.POLAK_RIBIERE){
			beta = Math.max(0, r_new.dot(r_new.sub(r_old))/delta_old);
		}
		d = r_new.add(d.mul(beta));
		if(++countRestart==restartIterations||r_new.dot(d)<=0){
			d = r_new;
			countRestart = 0;
		}
		if(delta_new>search_err*delta_init){
			iter=(int)1e20;
		}
	}

}
