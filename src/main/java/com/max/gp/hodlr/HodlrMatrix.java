package com.max.gp.hodlr;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.jblas.Decompose;
import org.jblas.DoubleMatrix;
import org.jblas.Solve;

import com.max.gp.kern.Kernel;
import com.max.gp.kern.SquaredExponential;

public class HodlrMatrix {
	
	DoubleMatrix kernMat;
	
	public HodlrMatrix(DoubleMatrix kernMat_) {
		kernMat = kernMat_;
	}
	
	public double getMatrixEntry(int i, int j) 
	{
		return kernMat.get(i, j);
	}
	
	public DoubleMatrix getMatrix(int startRow, int startCol, int nRows, int nCols)
	{
		return kernMat.getRange(startRow, startRow+nRows, startCol, startCol+nCols);
	}
	
	public DoubleMatrix getMatrixRow(int startCol, int nCols, int rowInd)
	{
		return kernMat.getColumnRange(rowInd, startCol, startCol+nCols);
	}
	
	public DoubleMatrix getMatrixCol(int startRow, int nRows, int colInd)
	{
		return kernMat.getColumnRange(colInd, startRow, startRow+nRows);
	}
	
	public int getArgMaxOfAbsVec(DoubleMatrix v, ArrayList<Integer> notAllowedIndices, double[] max) 
	{
		int n      =   v.length;
		int index  =   0;
		max[0] =   Math.abs(v.get(0));
		for(int j=0; j<n; ++j){
			if(!notAllowedIndices.contains(j)){
				if(Math.abs(v.get(j))>Math.abs(max[0])){
					max[0]     =   v.get(j);
					index   =   j;
				}
			}
		}
		return index;
	}
	
	
}
