package com.max.gp.hodlr;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.jblas.Decompose;
import org.jblas.Decompose.LUDecomposition;
import org.jblas.DoubleMatrix;
import org.jblas.Solve;
import org.jblas.ranges.IntervalRange;
import org.jblas.ranges.Range;

import com.max.gp.GPrHodlr;
import com.max.gp.kern.KernelFactory;
import com.max.gp.kern.SquaredExponential;
import com.max.gp.opt.OptimizerFactory;

public class HodlrNode {

	private HodlrMatrix mat;

	public HodlrNode[] parent;
	public HodlrNode[] lchd, rchd;
	public int levelNum, nodeNumber, levelBasedNodeNumber;
	public int nStart, nSize;
	public DoubleMatrix K, LU;
	public DoubleMatrix KInv;
	public double determinant;

	public int lrnk, rrnk;
	public DoubleMatrix lU, rU;
	public DoubleMatrix lV, rV;
	public DoubleMatrix lUInv, rUInv;
	public DoubleMatrix lVInv, rVInv;

	public boolean isLeaf;

	public HodlrNode(
			HodlrMatrix mat, 
			int levelNum, int nodeNumber, int levelBasedNodeNumber,
			int nStart, int nSize) 
	{
		this.mat = mat;
		this.levelNum = levelNum;
		this.nodeNumber = nodeNumber;
		this.levelBasedNodeNumber = levelBasedNodeNumber;
		this.nStart = nStart;
		this.nSize = nSize;
		this.parent = null;
		this.lchd = null;
		this.rchd = null;
	}

	public void assembleMatrices(double lowRankTolerance, DoubleMatrix diagonal)
	{
		if(isLeaf){
			K = mat.getMatrix(nStart, nStart, nSize, nSize);
			for(int k=0;k<nSize;k++){
				K.put(k, k, diagonal.get(nStart+k));
			}
		}
		else{
			DoubleMatrix[] UV = new DoubleMatrix[]{lU, rV};
			lrnk = partialPivLU(lchd[0].nStart, rchd[0].nStart, lchd[0].nSize, rchd[0].nSize, 
					lowRankTolerance, UV);
			lU = UV[0];
			rV = UV[1];
			lV = UV[0].transpose();
			rU = UV[1].transpose();
			if(lV.rows==0)System.exit(-1);
			rrnk = lrnk;
		}
	}

	public void matrixMatrixProduct(DoubleMatrix[] x)
	{
		int n = x[0].columns;
		if(isLeaf){
			x[1].put(new IntervalRange(nStart, nStart+nSize), 
					new IntervalRange(0, n), 
					x[1].getRange(nStart, nStart+nSize, 0, n)
					.add(K.mmul(x[0].getRange(nStart, nStart+nSize, 0, n)))
					);
		}
		else{
			int ln = lchd[0].nStart, lm = lchd[0].nSize;
			int rn = rchd[0].nStart, rm = rchd[0].nSize;
			x[1].put(new IntervalRange(ln, ln+lm), 
					new IntervalRange(0, n), 
					x[1].getRange(ln, ln+lm, 0, n)
					.add(lU.mmul(rV.mmul(x[0].getRange(rn, rn+rm, 0, n))))
					);
			x[1].put(new IntervalRange(rn, rn+rm), 
					new IntervalRange(0, n), 
					x[1].getRange(rn, rn+rm, 0, n)
					.add(rU.mmul(lV.mmul(x[0].getRange(ln, ln+lm, 0, n))))
					);
		}
	}

	public void setUVInversion() 
	{
		lUInv = lU;
		rUInv = rU;
		lVInv = lV;
		rVInv = rV;
	}

	public void computeK() 
	{
		if(!isLeaf){
			int lm = lV.rows;
			int rm = rV.rows;
			K = DoubleMatrix.eye(lm+rm);
			try {
				K.put(new IntervalRange(0, rm), 
						new IntervalRange(rm, rm+lm), 
						rVInv.mmul(rUInv));
				K.put(new IntervalRange(rm, rm+lm), 
						new IntervalRange(0, rm), 
						lVInv.mmul(lUInv));
			} catch (Exception e) {
				System.err.println(rVInv.rows);
				e.printStackTrace();
			}
		}
	}

	public void computeInverse() 
	{
		try{
			KInv = Solve.solveSymmetric(K, DoubleMatrix.eye(K.rows));
			LU = Decompose.lu(K).u;
		}
		catch(Exception e){
			System.out.println(K.rows);
			e.printStackTrace();
		}
	}

	public void applyInverse(DoubleMatrix[] matrix, int mStart)
	{
		int n = matrix[0].columns;
		int start = nStart-mStart;
		if(isLeaf){
			matrix[0].put(new IntervalRange(start, start+nSize), 
					new IntervalRange(0, n), 
					KInv.mmul(matrix[0].getRange(start, start+nSize, 0, n)));
		}
		else{
			DoubleMatrix tmp = new DoubleMatrix(lrnk+rrnk, n);
			tmp.put(new IntervalRange(0, lrnk), 
					new IntervalRange(0, n), 
					rVInv.mmul(matrix[0].getRange(
							start+lchd[0].nSize, start+lchd[0].nSize+rchd[0].nSize, 0, n)));
			tmp.put(new IntervalRange(lrnk, lrnk+rrnk), 
					new IntervalRange(0, n), 
					lVInv.mmul(matrix[0].getRange(
							start, start+lchd[0].nSize, 0, n)));
			DoubleMatrix tmpSolve = KInv.mmul(tmp);
			matrix[0].put(new IntervalRange(start, start+lchd[0].nSize), 
					new IntervalRange(0, n), 
					matrix[0].getRange(start, start+lchd[0].nSize, 0, n)
					.sub(lUInv.mmul(tmpSolve.getRange(0, lrnk, 0, n))));
			matrix[0].put(new IntervalRange(start+lchd[0].nSize, start+lchd[0].nSize+rchd[0].nSize), 
					new IntervalRange(0, n), 
					matrix[0].getRange(start+lchd[0].nSize, start+lchd[0].nSize+rchd[0].nSize, 0, n)
					.sub(rUInv.mmul(tmpSolve.getRange(lrnk, lrnk+rrnk, 0, n))));
		}
	}

	public double computeDeterminant()
	{
		if(LU.rows>0){
			determinant = Math.log(Math.abs(LU.get(0, 0)));
			for(int i=1;i<LU.rows;i++){
				determinant += Math.log(Math.abs(LU.get(i, i)));
			}
		}
		return determinant;
	}

	public int partialPivLU(
			int startRow, int startCol, int nRows, int nCols, 
			double tolerance,
			DoubleMatrix[] UV) 
	{
		int computedRank = 0;
		int tolerRnk = 8;
		if(nCols<=tolerRnk){
			computedRank = nCols;
			UV[0] = mat.getMatrix(startRow, startCol, nRows, nCols);
			UV[1] = DoubleMatrix.eye(nCols);
			return computedRank;
		}
		else if(nRows<=tolerRnk){
			computedRank = nRows;
			UV[0] = DoubleMatrix.eye(nRows);
			UV[1] = mat.getMatrix(startRow, startCol, nRows, nCols);
			return computedRank;
		}

		ArrayList<Integer> rowIndex = new ArrayList<Integer>(); 
		ArrayList<Integer> colIndex = new ArrayList<Integer>();
		ArrayList<DoubleMatrix> u = new ArrayList<DoubleMatrix>();
		ArrayList<DoubleMatrix> v = new ArrayList<DoubleMatrix>();
		double Gamma = 0;
		double max[] = new double[]{0};
		double matrixNorm = 0;
		rowIndex.add(0);
		int pivot = 0;
		DoubleMatrix row, col;
		double rowSquaredNorm = 0, rowNorm = 0;
		double colSquaredNorm = 0, colNorm = 0;
		computedRank = 0;
		Random rand = new Random(hashCode());
		do{
			do{
				row = mat.getMatrixRow(startCol, nCols, startRow+rowIndex.get(rowIndex.size()-1));
				for(int l=0;l<computedRank;l++){
					row = row.sub(v.get(l).mul(u.get(l).get(rowIndex.get(rowIndex.size()-1))));
				}
				pivot = mat.getArgMaxOfAbsVec(row, colIndex, max);
				
				int maxTries = 100;
				int count = 0;
				int count1 = 0;

				while(Math.abs(max[0])<tolerance && count<maxTries){
					int newRowIndex = 0;
					rowIndex.remove(rowIndex.size()-1);
					do{
						newRowIndex = rand.nextInt(nRows);
						count1++;
					}while(count1<maxTries && rowIndex.contains(newRowIndex));
					count1 = 0;
					rowIndex.add(newRowIndex);
					row = mat.getMatrixRow(startCol, nCols, startRow+newRowIndex);
					for(int l=0;l<computedRank;l++){
						row = row.sub(v.get(l).mul(u.get(l).get(newRowIndex)));
					}
					pivot = mat.getArgMaxOfAbsVec(row, colIndex, max);
					count++;
				}
				if(count==maxTries)break;
				count = 0;
				colIndex.add(pivot);
				Gamma = 1.0/max[0];
				col = mat.getMatrixCol(startRow, nRows, startCol+pivot);
				for(int l=0;l<computedRank;l++){
					col = col.sub(u.get(l).mul(v.get(l).get(pivot)));
				}
				pivot = mat.getArgMaxOfAbsVec(col, rowIndex, new double[]{0});
				while(Math.abs(max[0])<tolerance&&count<maxTries){
					int newColIndex = 0;
					colIndex.remove(colIndex.size()-1);
					do{
						newColIndex = rand.nextInt(nCols);
						count1++;
					}while(count1<maxTries && colIndex.contains(newColIndex));
					count1 = 0;
					colIndex.add(newColIndex);
					col = mat.getMatrixCol(startRow, nRows, startCol+newColIndex);
					for(int l=0;l<computedRank;l++){
						col = col.sub(v.get(l).mul(u.get(l).get(newColIndex)));
					}
					pivot = mat.getArgMaxOfAbsVec(col, rowIndex, new double[]{0});
					count++;
				}
				if(count == maxTries)break;
				count = 0;
				rowIndex.add(pivot);
				u.add(col.mul(Gamma));
				v.add(row);
				rowNorm = row.norm2();
				rowSquaredNorm = rowNorm*rowNorm;
				colNorm = col.norm2();
				colSquaredNorm = colNorm*colNorm;
				matrixNorm += Gamma*Gamma*rowSquaredNorm*colSquaredNorm;
				for(int j=0;j<computedRank;j++){
					matrixNorm += 2*(u.get(j).dot(u.get(u.size()-1))*v.get(j).dot(v.get(v.size()-1)));
				}
				computedRank++;
			} while(rowNorm*colNorm>Math.abs(max[0])*tolerance*matrixNorm 
					&& computedRank <= Math.min(nRows, nCols));
			if(computedRank==0)tolerance/=10;
		}while(computedRank<1);
		if(computedRank>=Math.min(nRows, nCols)){
			if(nRows<nCols){
				computedRank = nRows;
				UV[0] = DoubleMatrix.eye(nRows);
				UV[1] = mat.getMatrix(startRow, startCol, nRows, nCols);
				return computedRank;
			}
			else{
				computedRank = nCols;
				UV[0] = mat.getMatrix(startRow, startCol, nRows, nCols);
				UV[1] = DoubleMatrix.eye(nCols);
				return computedRank;
			}
		}
		UV[0] = new DoubleMatrix(nRows, computedRank);
		UV[1] = new DoubleMatrix(computedRank, nCols);
		for(int j=0;j<computedRank;j++){
			UV[0].putColumn(j, u.get(j));
			UV[1].putRow(j, v.get(j));
		}
		return computedRank;
	}
	
	public static void main(String[] args) {
		
		int N = 50;
		double tolerance = 1E-2;
		// Gaussian Kernel Matrix
		Random rand = new Random();
		DoubleMatrix X = new DoubleMatrix(N);
		DoubleMatrix Y = new DoubleMatrix(N);
		for(int i=0;i<N;i++){
			double x = i*0.2+0.05*rand.nextFloat();
			double y = Math.cos(x)+0.1*rand.nextFloat();
			X.put(i, x);
			Y.put(i, y);
		}
		SquaredExponential se = new SquaredExponential(1, false);
		se.setHyperParams(new DoubleMatrix(new double[][]{{0.1, 2,0.3}}));
		DoubleMatrix mat = se.computeMatrix(X);
		HodlrMatrix hmat = new HodlrMatrix(mat);
		HodlrNode test = new HodlrNode(hmat, 1, 1, 1, 0, N);
		DoubleMatrix UV[] = new DoubleMatrix[2];
		System.out.println(test.partialPivLU(0, 0, N, N, tolerance, UV));
		mat.print();
		UV[0].mmul(UV[1]).print();
	}

}
