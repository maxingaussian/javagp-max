package com.max.gp.hodlr;

import java.util.Random;

import org.jblas.Decompose;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;
import org.jblas.Solve;

import com.max.gp.kern.Kernel;
import com.max.gp.kern.Periodic;
import com.max.gp.kern.SquaredExponential;
import com.max.gp.alg.TimeWatch;

public class HodlrTest {
	
	static void test() {
		int N = 1000;
		int nLeaf = 500;
		double tolerance = 1E-16;
		
		// Gaussian Kernel Matrix
		Random rand = new Random();
		DoubleMatrix X = new DoubleMatrix(N);
		DoubleMatrix Y = new DoubleMatrix(N);
		for(int i=0;i<N;i++){
			double x = i*0.2+0.05*rand.nextFloat();
			double y = Math.cos(x)+0.1*rand.nextFloat();
			X.put(i, x);
			Y.put(i, y);
		}
		Kernel se = new Periodic(1, false);
		se.setHyperParams(new DoubleMatrix(new double[][]{{0.1,2,0.3,0.5}}));
		DoubleMatrix mat = se.computeMatrix(X);
		
		
		TimeWatch watch = null;
		double time = 0;
		// HODLR Initialization
		watch = new TimeWatch();
		HodlrMatrix K = new HodlrMatrix(mat);
		HodlrTree A = new HodlrTree(K, mat.rows, nLeaf);
		DoubleMatrix diagonal = mat.diag();
		A.assembleMatrix(diagonal, tolerance);
		A.computeFactor();
		time = watch.time();
		A.diagnostics();
		A.displayAllRanks();
		A.totalCalls();
		System.out.println("HODLR Initialization Costs "+time+"ms.");

		// Matrix-Matrix Product Test
		DoubleMatrix x = DoubleMatrix.rand(N, 1);
		// Exact:
		DoubleMatrix bExact = new DoubleMatrix(N,1);
		watch = new TimeWatch();
		bExact = mat.mmul(x);
		time = watch.time();
		System.out.println("Exact Matrix-Matrix Product Costs "+time+"ms.");
		// Fast:
		DoubleMatrix bFast = new DoubleMatrix(N,1);
		watch = new TimeWatch();
		bFast = A.matMatProduct(x);
		time = watch.time();
		System.out.println("Fast Matrix-Matrix Product Costs "+time+"ms.");
		System.out.println("Fast Matrix-Matrix Product Differs "+bExact.sub(bFast).norm1()/x.length/bExact.mean());
		
		// Solving Linear System Test*
		watch = new TimeWatch();
		DoubleMatrix xExact = Solve.solvePositive(mat, bExact);
		time = watch.time();
		System.out.println("Exact Solving Linear System Costs "+time+"ms.");
		System.out.println("Exact Solving Linear System Differs "+x.sub(xExact).norm1()/x.length/x.mean());
		watch = new TimeWatch();
		DoubleMatrix xFast = A.solve(bExact);
		time = watch.time();
		System.out.println("Fast Solving Linear System Costs "+time+"ms.");
		System.out.println("Fast Solving Linear System Differs "+x.sub(xFast).norm1()/x.length/x.mean());
		
		// Log Determinant Test*
		watch = new TimeWatch();
		double dExact = MatrixFunctions.log(Decompose.cholesky(mat).diag()).sum()*2;
		time = watch.time();
		System.out.println("Exact Log Determinant Costs "+time+"ms.");
		watch = new TimeWatch();
		double dFast = A.computeDeterminant();
		time = watch.time();
		System.out.println("Fast Log Determinant Costs "+time+"ms.");
		System.out.println("Fast Log Determinant Differs "+Math.abs(dExact-dFast));
	}

	public static void main(String[] args) {
		test();
	}

}
