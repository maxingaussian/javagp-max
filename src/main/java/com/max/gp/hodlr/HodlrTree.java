package com.max.gp.hodlr;

import java.util.ArrayList;

import org.jblas.DoubleMatrix;


public class HodlrTree {

	public HodlrMatrix mat;
	private HodlrNode[] root;
	int N, nLeaf;
	double lowRankTolerance, determinant;
	DoubleMatrix diagonal;
	int nLevels, nNodes;
	ArrayList<ArrayList<Integer[]>> ranks;
	ArrayList<ArrayList<Integer>> calls;
	int callsNum;
	
	public HodlrTree(HodlrMatrix mat, int N, int nLeaf) 
	{
		this.mat = mat;
		this.N = N;
		this.nLeaf = nLeaf;
		this.root = new HodlrNode[]{new HodlrNode(mat, 0, 0, 0, 0, N)};
		this.nNodes = 0;
		createTree(root);
		ranks = new ArrayList<ArrayList<Integer[]>>();
		calls = new ArrayList<ArrayList<Integer>>();
		this.nLevels = (int) (Math.log(nNodes)/Math.log(2));
	}
	
	void createTree(HodlrNode[] node)
	{
		nNodes++;
		if(node[0].nSize<=nLeaf){
			node[0].isLeaf = true;
		}
		else{
			node[0].isLeaf = false;
			int n0 = node[0].nSize/2;
			int n1 = node[0].nSize-n0;
			node[0].lchd = new HodlrNode[]{new HodlrNode(mat, node[0].levelNum+1, 0, 
					2*node[0].levelBasedNodeNumber, node[0].nStart, n0)};
			node[0].lchd[0].parent = node;
			node[0].rchd = new HodlrNode[]{new HodlrNode(mat, node[0].levelNum+1, 1, 
					2*node[0].levelBasedNodeNumber+1, node[0].nStart+n0, n1)};
			node[0].rchd[0].parent = node;
			createTree(node[0].lchd);
			createTree(node[0].rchd);
		}
	}
	
	void assembleMatrix(HodlrNode[] node)
	{
		if(node!=null){
			node[0].assembleMatrices(lowRankTolerance, diagonal);
			assembleMatrix(node[0].lchd);
			assembleMatrix(node[0].rchd);
		}
	}
	
	void obtainAllRanks(HodlrNode[] node)
	{
		if(node!=null){
			if(ranks.size()<node[0].levelNum+1){
				ArrayList<Integer[]> temp = new ArrayList<Integer[]>();
				ranks.add(temp);
				ArrayList<Integer> temp1 = new ArrayList<Integer>();
				calls.add(temp1);
			}
			Integer[] temp = new Integer[2];
			temp[0] = node[0].lrnk;
			temp[1] = node[0].rrnk;
			ranks.get(node[0].levelNum).add(temp);
			Integer temp1 = node[0].nSize*(temp[0]+temp[1])-temp[0]*temp[0]-temp[1]*temp[1];
			calls.get(node[0].levelNum).add(temp1);
			obtainAllRanks(node[0].lchd);
			obtainAllRanks(node[0].rchd);
		}
	}

	void matMatProduct(HodlrNode[] node, DoubleMatrix[] x)
	{
		if(node!=null){
			node[0].matrixMatrixProduct(x);
			matMatProduct(node[0].lchd, x);
			matMatProduct(node[0].rchd, x);
		}
	}
	
	void computeFactor(HodlrNode[] node)
	{
		if(node!=null){
			computeFactor(node[0].lchd);
			computeFactor(node[0].rchd);
			node[0].computeK();
			node[0].computeInverse();
			HodlrNode[] parent = node[0].parent;
			int num = node[0].nodeNumber;
			int mStart = node[0].nStart;
			while(parent!=null){
				DoubleMatrix tmp[] = new DoubleMatrix[1];
				if(num==0){
					tmp[0] = parent[0].lUInv;
				}
				else if(num==1){
					tmp[0] = parent[0].rUInv;
				}
				node[0].applyInverse(tmp, mStart);
				if(num==0){
					parent[0].lUInv = tmp[0];
				}
				else if(num==1){
					parent[0].rUInv = tmp[0];
				}
				num = parent[0].nodeNumber;
				mStart = parent[0].nStart;
				parent = parent[0].parent;
			}
		}
	}
	
	void setMatricesForInversion(HodlrNode[] node)
	{
		if(node!=null){
			node[0].setUVInversion();
			setMatricesForInversion(node[0].lchd);
			setMatricesForInversion(node[0].rchd);
		}
	}
	
	void solve(HodlrNode[] node, DoubleMatrix[] x)
	{
		if(node!=null){
			solve(node[0].lchd, x);
			solve(node[0].rchd, x);
			node[0].applyInverse(x, 0);
		}
	}
	
	void computeDeterminant(HodlrNode[] node)
	{
		if(node!=null){
			computeDeterminant(node[0].lchd);
			node[0].computeDeterminant();
			computeDeterminant(node[0].rchd);
			node[0].computeDeterminant();
			determinant = determinant+node[0].determinant;
		}
	}
	
	public void assembleMatrix(DoubleMatrix diagonal, double lowRankTolerance)
	{
		this.lowRankTolerance = lowRankTolerance;
		this.diagonal = diagonal;
		assembleMatrix(root);
	}
	
	public DoubleMatrix matMatProduct(DoubleMatrix x)
	{
		DoubleMatrix tmp[] = new DoubleMatrix[]{x, DoubleMatrix.zeros(N, x.columns)};
		matMatProduct(root, tmp);
		return tmp[1];
	}
	
	public void computeFactor()
	{
		setMatricesForInversion(root);
		computeFactor(root);
	}
	
	public DoubleMatrix solve(DoubleMatrix b)
	{
		DoubleMatrix[] x = new DoubleMatrix[]{b.dup()};
		solve(root, x);
		return x[0];
	}
	
	public double computeDeterminant()
	{
		determinant = 0;
		computeDeterminant(root);
		return determinant;
	}
	
	public void diagnostics()
	{
		obtainAllRanks(root);
		ArrayList<Integer> temp = new ArrayList<Integer>();
		int nLeaves = (int) Math.pow(2, nLevels);
		for(int i=0;i<nLeaves;i++){
			temp.add(nLeaf*nLeaf);
		}
		calls.add(temp);
		callsNum = 0;
		for(int i=0;i<nLevels;i++){
			int nNodes = (int) Math.pow(2, i);
			for(int j=0;j<nNodes;j++){
				callsNum += calls.get(i).get(j);
			}
		}
	}
	
	public void displayAllRanks() 
	{
		System.out.println("DISPLAYING ALL OFF-DIAGONAL RANKS BASED ON LEVEL NUMBER AND NODE NUMBER BASED ON THE ORDERING SHOWN BELOW.");
		System.out.println("(LEVEL NUMBER, NODE NUMBER) (TOP-RIGHT OFF-DIAGONAL BLOCK RANK, BOTTOM-LEFT OFF-DIAGONAL BLOCK RANK)");
		for (int i=0; i<nLevels; ++i) {
			int nNodes = (int) Math.pow(2, i);
            for (int j=0; j<nNodes; ++j) {
                    System.out.println("( "+i+", "+j+") "+"( "+ranks.get(i).get(j)[0]+", "+ranks.get(i).get(j)[1]+")");
            }
		}
	}
	
	public void displayRank(int i, int j)
	{
		obtainAllRanks(root);
		System.out.println("Rank of the top-right off diagonal block for the "+j+"th diagonal block at level "+i+" is "+ranks.get(i).get(j)[0]);
		System.out.println("Rank of the bottom-left off diagonal block for the "+j+"th diagonal block at level "+i+" is "+ranks.get(i).get(j)[1]);
	}
	
	public void totalCalls()
	{
		System.out.println("Estimate of total number of calls to getMatrixEntry: "+callsNum);
	}
	
	public void totalCalls(int i, int j)
	{
		System.out.println("Estimate of total number of calls to getMatrixEntry for the "+j+"th diagonal block at level "+i+": "+calls.get(i).get(j));
	}
	
}
