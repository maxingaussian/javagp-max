package com.max.gp.hodlr;

import org.jblas.Decompose;
import org.jblas.DoubleMatrix;

public class Hodlr {

	double tol = 1E-13;
	int n, leaf = 100;
	HodlrTree hodlr;
	
	public Hodlr(DoubleMatrix kernMat, boolean factorize) {
		n = kernMat.rows;
		leaf = n/5;
		hodlr = new HodlrTree(new HodlrMatrix(kernMat), n, leaf);
		hodlr.assembleMatrix(kernMat.diag(), tol);
		if(factorize){
			while(true){
				try {
					hodlr.computeFactor();
					break;
				} catch (Exception e) {
					kernMat = kernMat.add(1E-2);
					continue;
				}
			}
		}
	}
	
	public double trace(DoubleMatrix A, double tol) {
		int samples = 1;
		double trace = 0, sum = 0, tmp = 0;
		DoubleMatrix z = DoubleMatrix.randn(n);
		tmp = solve(z).dot(A.mmul(z));
		do{
			samples++;
			sum += tmp;
			z = DoubleMatrix.randn(n);
			tmp = solve(z).dot(A.mmul(z));
			System.out.println("Sample "+samples+": "+Math.abs(sum/(samples-1)-(sum+tmp)/samples));
		}while(Math.abs(sum/(samples-1)-(sum+tmp)/samples)>tol&&samples<500);
		if(samples==500)return 1E10;
		trace = (sum+tmp)/samples;
		return trace;
	}
	
	public DoubleMatrix mat() {
		return hodlr.mat.kernMat;
	}
	
	public DoubleMatrix solve(DoubleMatrix b) {
		return hodlr.solve(b);
	}
	
	public DoubleMatrix mmul(DoubleMatrix x) {
		return hodlr.matMatProduct(x);
	}
	
	public double getlogDet() {
		return hodlr.computeDeterminant();
	}
	
	public DoubleMatrix inv() {
		return hodlr.solve(DoubleMatrix.eye(n));
	}
	
	public DoubleMatrix sqrt() {
		return Decompose.cholesky(hodlr.mat.kernMat);
	}

}
