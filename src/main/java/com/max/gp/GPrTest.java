package com.max.gp;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;
import org.jblas.ranges.IntervalRange;

import com.max.gp.alg.TimeWatch;
import com.max.gp.data.Dataset;
import com.max.gp.data.Regression;
import com.max.gp.kern.Kernel;
import com.max.gp.kern.KernelFactory;
import com.max.gp.opt.OptimizerFactory;

public class GPrTest {

	static void test() {
		double time = 0;
		TimeWatch watch = null;
		Dataset.loadFullData(Regression.HOUSING);
		DoubleMatrix XFull = Dataset.getInputData();
		DoubleMatrix YFull = Dataset.getOutputData();
		int M = XFull.rows;
		double testProportion = 0.7;
		int N = (int) (testProportion*M);
		DoubleMatrix X = XFull.getRows(new IntervalRange(0, N));
		DoubleMatrix Y = YFull.getRows(new IntervalRange(0, N));
		DoubleMatrix Xt = XFull.getRows(new IntervalRange(N, M));
		DoubleMatrix Yt = YFull.getRows(new IntervalRange(N, M));
		
//		watch = new TimeWatch();
//		GPr testFull = new GPrFull(KernelFactory.SEiso(X.columns));
//		testFull.train(X, Y);
//		testFull.optimize(OptimizerFactory.iRpropPlus(), 3, 100, true);
//		time = watch.time();
//		System.out.println("GPrFull training+optimization Costs "+time+"ms.");
//		testFull.plotData(0);
//		DoubleMatrix Yp = testFull.predict(Xt, false, false).get(0);
//		Yp.print();
//		Yt.print();
//		double mse = MatrixFunctions.pow(Yp.sub(Yt),2).sum()/(M-N);
//		System.out.println("GPrFull MSE = "+mse);
//		testFull.getParams().print();
		
		watch = new TimeWatch();
		Kernel kernel = KernelFactory.Prod(KernelFactory.SEard(X.columns), KernelFactory.PERard(X.columns));
		GPr testHodlr = new GPrHodlr(kernel);
		System.out.println(testHodlr.getKernel().getHyperParamsSize());
		testHodlr.train(X, Y);
		testHodlr.optimize(OptimizerFactory.iRpropPlus(), 5, 100, true);
		time = watch.time();
		System.out.println("N = "+N);
		System.out.println("GPrHodlr training+optimization Costs "+time+"ms.");
		DoubleMatrix Yp2 = testHodlr.predict(Xt, false, false).get(0);
		Yp2.print();
		Yt.print();
		double mse = MatrixFunctions.pow(Yp2.sub(Yt),2).sum()/(M-N);
		System.out.println("GPrHodlr MSE = "+mse);
	}

	public static void main(String[] args) {
		test();
	}

}
