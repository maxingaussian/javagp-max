package com.max.gp.alg;

import org.jblas.DoubleMatrix;

import com.max.gp.kern.KernelFactory;

public class Algorithm {

	public static double computeTrace(DoubleMatrix KInv, DoubleMatrix A, double tol) {
		int samples = 1, n = KInv.rows;
		double trace = 0, sum = 0, tmp = 0;
		DoubleMatrix z = DoubleMatrix.randn(n);
		tmp = KInv.mmul(z).dot(A.mmul(z));
		do{
			samples++;
			sum += tmp;
			z = DoubleMatrix.randn(n);
			tmp = KInv.mmul(z).dot(A.mmul(z));
//			System.out.println("Sample "+samples+": "+Math.abs(sum/(samples-1)-(sum+tmp)/samples));
		}while(Math.abs(sum/(samples-1)-(sum+tmp)/samples)>tol);
		trace = (sum+tmp)/samples;
		return trace;
	}
	
	public static DoubleMatrix updateL(DoubleMatrix L, DoubleMatrix Km, DoubleMatrix Kmn) {
		int n = L.rows+Km.rows, m = Km.rows;
		int diff = n-m;
		L=DoubleMatrix.concatHorizontally(L, DoubleMatrix.zeros(diff, m));
		L=DoubleMatrix.concatVertically(L, DoubleMatrix.zeros(m, n));
		double tmp1[] = Km.data;
		double tmp2[] = Kmn.data;
		double el[] = L.data;
		for(int i = diff; i< n;i++){
			for(int k = 0; k < (i+1); k++){
				double sum = 0;
				for(int j = 0; j < k; j++){
					sum += el[i*n+j] * el[k*n+j];
				}
				if(i==k){
					el[i*n+k] = Math.sqrt(tmp1[(i-diff)*m+(i-diff)] - sum);
				}
				else{
					double a_ik = 0;
					if(k<diff){
						a_ik = tmp2[(i-diff)*diff+k];
					}else{
						a_ik = tmp1[(i-diff)*m+k-diff];
					}
					el[i*n+k] = (1.0 / el[k*n+k] * (a_ik-sum));
				}
			}
		}
		return L;
	}
	
	public static DoubleMatrix updateLInv(DoubleMatrix LInv, DoubleMatrix L) {
		int n = L.rows, diff = LInv.rows;
		int m = n-diff;
		LInv=DoubleMatrix.concatHorizontally(LInv, DoubleMatrix.zeros(diff, m));
		LInv=DoubleMatrix.concatVertically(LInv, DoubleMatrix.zeros(m, n));
		double el[] = L.data;
		double einv[] = LInv.data;
		for(int i=diff;i<n;i++){
			for(int j=0;j<i;j++){
				einv[i*n+j] = 0;
				for(int k=0;k<i;k++){
					einv[i*n+j]-=el[i*n+k]*einv[k*n+j];
				}
				einv[i*n+j]/=el[i*n+i];
			}
			einv[i*n+i]=1./el[i*n+i];
		}
		return LInv;
	}
	
	public static void main(String[] args) {
		int n = 5000;
		DoubleMatrix X = DoubleMatrix.randn(n);
		DoubleMatrix K = KernelFactory.SE().computeMatrix(X);
		System.out.println(computeTrace(K, DoubleMatrix.eye(n), 0.01));
		System.out.println(K.diag().sum());
	}
	
}
