package com.max.gp.data;

import java.util.Random;

import org.jblas.DoubleMatrix;
import org.jblas.ranges.IntervalRange;

public class Dataset {

	static DoubleMatrix fullData = null;
	static Random rand = new Random();
	
	static DoubleMatrix sinx(int m) {
		DoubleMatrix X = new DoubleMatrix(m);
		DoubleMatrix Y = new DoubleMatrix(m);
		for(int i=0;i<m;i++){
			double x = i*1+0.1*rand.nextFloat();
			double y = Math.cos(x/20)+0.01*rand.nextFloat();
			X.put(i, x);
			Y.put(i, y);
		}
		return DoubleMatrix.concatHorizontally(X, Y);
	}
	
	public static void loadFullData(Object dataName) {
		try {
			if (dataName instanceof Regression) {
				switch ((Regression)dataName) {
				case SINX:
					fullData = sinx(100);
					break;
				case HOUSING:
					fullData = DoubleMatrix.loadAsciiFile("data/regression/housing");
					break;
				case AIRFOIL:
					fullData = DoubleMatrix.loadAsciiFile("data/regression/airfoil");
					break;
				case CONCRETE:
					fullData = DoubleMatrix.loadAsciiFile("data/regression/concrete");
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(int i=0;i<fullData.rows;i++){
			fullData.swapRows(i, rand.nextInt(fullData.rows));
		}
	}

	public static DoubleMatrix getInputData() {
		DoubleMatrix inputData = null;
		inputData = fullData.getColumns(new IntervalRange(0, fullData.columns-1));
		return inputData;
	}
	
	public static DoubleMatrix getOutputData() {
		DoubleMatrix outputData = null;
		outputData = fullData.getColumn(fullData.columns-1);
		return outputData;
	}
	
	public static void main(String[] args) {
	}
	
}
