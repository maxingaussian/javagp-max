package com.max.gp.data;

public enum Regression {
	
	SINX,
	// https://archive.ics.uci.edu/ml/datasets/Housing
	HOUSING,
	// http://archive.ics.uci.edu/ml/datasets/Airfoil+Self-Noise
	AIRFOIL,
	// http://archive.ics.uci.edu/ml/datasets/Concrete+Compressive+Strength
	CONCRETE
}
