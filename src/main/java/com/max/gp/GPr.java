/**
 * @author Max W. Y. Lam
 *
 */
package com.max.gp;

import java.util.ArrayList;

import org.jblas.DoubleMatrix;

import com.max.gp.kern.Kernel;
import com.max.gp.opt.Optimizer;

public interface GPr {
	
	public boolean isTrained();
	public DoubleMatrix getX();
	public DoubleMatrix getY();
	public DoubleMatrix getL();
	public double getNegativeLogLikelihood();
	public DoubleMatrix getNegativeLogLikelihoodGradient();
	public void setKernel(Kernel kern);
	public Kernel getKernel();
	public void setParams(DoubleMatrix params);
	public DoubleMatrix getParams();
	public void train(DoubleMatrix Xn, DoubleMatrix Yn);
	public void optimize(Optimizer opt, int samples, int iterations, boolean message);
	public void plotData(int d);
	public ArrayList<DoubleMatrix> predict(DoubleMatrix Xm, boolean var, boolean prob);
	
}
