package com.max.gp;

import java.util.ArrayList;
import java.util.function.Function;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import com.max.gp.hodlr.Hodlr;
import com.max.gp.kern.Kernel;
import com.max.gp.opt.Optimizer;
import com.max.gp.plot.PlotTool;

public class GPrHodlr implements GPr {

	public final static double log2pi = Math.log(2*Math.PI);  

	int n, m;
	Kernel kern;
	Hodlr K;
	DoubleMatrix Xn, Yn, alpha;
	DoubleMatrix Xmean, Xstd, Xmin, Xmax;
	double Ymean, Ystd;


	public GPrHodlr(Kernel kern_) {
		Xn = Yn = null;
		K = null;
		n = m = 0;
		Xmean = Xstd = null;
		Ymean = Ystd = 0;
		kern = kern_;
	}

	public boolean isTrained() {
		return K!=null;
	}

	public DoubleMatrix getX(){
		return Xn.mulRowVector(Xstd).addRowVector(Xmean);
	}

	public DoubleMatrix getY(){
		return Yn.mul(Ystd).add(Ymean);
	}

	public DoubleMatrix getL(){
		return K.sqrt();
	}
	
	public double getLikelihood() {
		double logLik = -1*getNegativeLogLikelihood();
		return Math.exp(logLik);
	}

	public double getNegativeLogLikelihood() {
		double Yalpha = Yn.dot(alpha);
		double logdetK = K.getlogDet();
		double constant = n*log2pi;
		return (Yalpha+logdetK+constant);
	}

	public DoubleMatrix getNegativeLogLikelihoodGradient() {
			DoubleMatrix grad = new DoubleMatrix(getParamsSize(), 1);
			for(int i=0;i<grad.length;i++){
				DoubleMatrix A = kern.computeDerivativeMatrix(Xn, i);
				grad.put(i, 0, K.solve(A).diag().sum()-alpha.dot(A.mmul(alpha)));
			}
			return grad;
	}

	public int getParamsSize() {
		return kern.getHyperParamsSize(); // output noise
	}

	public void setKernel(Kernel kern_) {
		kern = kern_;
	}

	public Kernel getKernel() {
		return kern;
	}

	public void setParams(DoubleMatrix params_) {
		kern.setHyperParams(params_);
		DoubleMatrix Kn = kern.computeMatrix(Xn);
		K = new Hodlr(Kn, true);
		alpha = K.solve(Yn);
	}

	public DoubleMatrix getParams() {
		return kern.getHyperParams();
	}

	public void train(DoubleMatrix Xn_, DoubleMatrix Yn_) {
		n = Xn_.rows;
		m = Xn_.columns;
		Xn = Xn_;
		Yn = Yn_;
		Xmin = Xn.columnMins();
		Xmax = Xn.columnMaxs();
		Xmean = Xn.columnMeans(); 
		Ymean = Yn.mean();
		Xstd = MatrixFunctions.sqrt(MatrixFunctions.pow(Xn.subRowVector(Xmean),2).columnMeans());
		Ystd = Yn.norm2()/Math.sqrt(n);
		Xn = (Xn.subRowVector(Xmean)).divRowVector(Xstd);
		Yn = (Yn.sub(Ymean)).div(Ystd);
		setParams(DoubleMatrix.rand(getParamsSize()));
	}

	public void optimize(Optimizer opt, int samples, int iterations, boolean message) {
		opt.init(getParamsSize(), 
				new Function<DoubleMatrix, Void>() {
			public Void apply(DoubleMatrix params) {
				setParams(params);
				return null;
			}
		},
				new Function<Void, Double>() {
			public Double apply(Void t) {
				double nll = getNegativeLogLikelihood();
				if(Double.isFinite(nll))
					return nll;
				return 1E10;
			}
		},
				new Function<Void, DoubleMatrix>() {
			public DoubleMatrix apply(Void t) {
				return getNegativeLogLikelihoodGradient();
			}
		});
		opt.run(samples, iterations, message);
	}

	public ArrayList<DoubleMatrix> predict(DoubleMatrix Xs, boolean var, boolean prob) {
		Xs = (Xs.subRowVector(Xmean)).divRowVector(Xstd);
		DoubleMatrix Ksn = kern.computeMatrix(Xs, Xn);
		ArrayList<DoubleMatrix> res = new ArrayList<DoubleMatrix>();
		res.add(Ksn.mmul(alpha).mul(Ystd).add(Ymean));
		if(var){
			DoubleMatrix Kss = kern.computeMatrix(Xs.getColumn(0));
			for(int d=1;d<m;d++){
				Kss = Kss.mul(kern.computeMatrix(Xs.getColumn(d)));
			}
			res.add(Kss.sub(Ksn.mmul(K.solve(Ksn.transpose()))));
		}
		return res;
	}

	public void plotData(int d) {
		PlotTool.plotGPwithData(this, d, Xmin.get(d)-1, Xmax.get(d)+1);
	}


}
