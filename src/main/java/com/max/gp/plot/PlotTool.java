/**
 * @author Max W. Y. Lam
 */
package com.max.gp.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.jblas.Decompose;
import org.jblas.DoubleMatrix;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYDifferenceRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import org.jfree.util.ShapeUtilities;

import com.max.gp.GPr;
import com.max.gp.kern.Kernel;

public final class PlotTool {

	public static void plotKernelwithSamples(Kernel kern, int samples){
		int pts = 1000;
		double ci[] = new double[]{0.13,0.25,0.39,0.52,0.67,0.84,1.04,1.28,1.64,2.2};
		XYSeries up []= new XYSeries[ci.length];
		XYSeries low []= new XYSeries[ci.length];
		for(int i=0;i<ci.length;i++){
			up[i] = new XYSeries("Upper Bound");
			low[i] = new XYSeries("Lower Bound");
		}
		DoubleMatrix Xplot = DoubleMatrix.linspace(0, 1, pts);
		for(int i=0;i<pts;i++){
			for(int j=0;j<ci.length;j++){
				up[j].add(Xplot.get(i),ci[j]*1.2);
				low[j].add(Xplot.get(i),-ci[j]*1.2);
			}
		}
		DoubleMatrix K = kern.computeMatrix(Xplot);
		DoubleMatrix L = Decompose.cholesky(K).transpose();
		XYSeriesCollection simplesData = new XYSeriesCollection();
		for(int i=0;i<samples;i++){
			XYSeries sample_i = new XYSeries("Sample "+Integer.toString(i+1));
			DoubleMatrix Y = L.mmul(DoubleMatrix.randn(pts, pts));
			for(int j=0;j<pts;j++){
				double x=Xplot.get(j);
				double y=Y.get(j);
				sample_i.add(x,y);
			}
			simplesData.addSeries(sample_i);
		}
		JFreeChart chart = ChartFactory.createXYLineChart
				("Kernel Draw Samples", 
						"X", "Y", simplesData, 
						PlotOrientation.VERTICAL, 
						false, true, false);
		XYPlot addLines = chart.getXYPlot();
		XYSeriesCollection diff[] = new XYSeriesCollection[ci.length];
		XYDifferenceRenderer fill[] = new XYDifferenceRenderer[ci.length];
		for(int i=0;i<ci.length;i++){
			diff[i] = new XYSeriesCollection();
			diff[i].addSeries(up[i]);
			diff[i].addSeries(low[i]);
			fill[i] = new XYDifferenceRenderer(); 
			fill[i].setPositivePaint(new Color(0x00,0x20,0x50,(int)(10*(3.8-ci[i]))));
			addLines.setDataset(1+i, diff[i]);
			addLines.setRenderer(1+i, fill[i]);
			addLines.getRenderer(1+i).setSeriesPaint(0, fill[i].getPositivePaint());
			addLines.getRenderer(1+i).setSeriesPaint(1, fill[i].getPositivePaint());
		}
		addLines.setBackgroundPaint(ChartColor.WHITE);
		addLines.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
		final NumberAxis rangeAxis = (NumberAxis) addLines.getRangeAxis();
		rangeAxis.setRange(-4, 4);
		final NumberAxis domainAxis = (NumberAxis) addLines.getDomainAxis();
		domainAxis.setRange(0, 1);
		ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        JFrame plot = new JFrame("Kernel Plot");
        plot.setContentPane(chartPanel);
        plot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        plot.pack();
        RefineryUtilities.centerFrameOnScreen(plot);
        plot.setVisible(true);
	}
	
	public static void plotGPwithData(GPr gPr, int d, double xi, double xj){
		DoubleMatrix X = gPr.getX().getColumn(d);
		DoubleMatrix Y = gPr.getY();
		XYSeries mu = new XYSeries("Mean Function");
		XYSeries train = new XYSeries("Training Data");
		int pts = 500;
		DoubleMatrix Xplot = DoubleMatrix.linspace((int)(xi)-1, (int)(xj)+1, pts);
		ArrayList<DoubleMatrix> res = gPr.predict(Xplot, true, false);
		double ci[] = new double[]{0.13,0.25,0.39,0.52,0.67,0.84,1.04,1.28,1.64,2.2};
		XYSeries up []= new XYSeries[ci.length];
		XYSeries low []= new XYSeries[ci.length];
		for(int i=0;i<ci.length;i++){
			up[i] = new XYSeries("Upper Bound");
			low[i] = new XYSeries("Lower Bound");
		}
		for(int i=0;i<pts;i++){
			double x=Xplot.get(i, 0);
			double y=res.get(0).get(i);
			double s=res.get(1).diag().get(i);
			mu.add(x,y);
			for(int j=0;j<ci.length;j++){
				up[j].add(x,y+s*ci[j]*1.2);
				low[j].add(x,y-s*ci[j]*1.2);
			}
		}
		for(int i=0;i<X.rows;i++){
			train.add(X.get(i, 0), Y.get(i, 0));
		}
		XYSeriesCollection data = new XYSeriesCollection(mu);
		XYSeriesCollection diff[] = new XYSeriesCollection[ci.length];
		XYDifferenceRenderer fill[] = new XYDifferenceRenderer[ci.length];
		for(int i=0;i<ci.length;i++){
			diff[i] = new XYSeriesCollection();
			diff[i].addSeries(up[i]);
			diff[i].addSeries(low[i]);
			fill[i] = new XYDifferenceRenderer(); 
			fill[i].setPositivePaint(new Color(0x00,0x20,0x50,(int)(10*(3.8-ci[i]))));
		}
		XYSeriesCollection trainData = new XYSeriesCollection(train);
		JFreeChart chart = ChartFactory.createXYLineChart
				("Gaussian Process Regression", 
						"X", "Y", trainData, 
						PlotOrientation.VERTICAL, 
						false, true, false);
		XYPlot addLines = chart.getXYPlot();
		addLines.setBackgroundPaint(ChartColor.WHITE);
		addLines.setDataset(1, data);
		for(int i=0;i<ci.length;i++){
			addLines.setDataset(2+i, diff[i]);
			addLines.setRenderer(2+i, fill[i]);
			addLines.getRenderer(2+i).setSeriesPaint(0, fill[i].getPositivePaint());
			addLines.getRenderer(2+i).setSeriesPaint(1, fill[i].getPositivePaint());
		}
		addLines.setRenderer(1, new StandardXYItemRenderer());
		XYLineAndShapeRenderer ren = new XYLineAndShapeRenderer();
		ren.setSeriesLinesVisible(0, false);
		ren.setSeriesShapesVisible(0, true);
		ren.setSeriesShape(0, ShapeUtilities.createDiagonalCross(3, 1));
		addLines.setRenderer(0, ren);
		addLines.getRenderer(0).setSeriesItemLabelsVisible(0, false);
		addLines.getRenderer(1).setSeriesPaint(0, new Color(0x00,0x20,0x50,88));
		addLines.getRenderer(1).setSeriesStroke(
			    0, 
			    new BasicStroke(
		                3.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 
		                2.0f, new float[] {10.0f, 6.0f}, 0.0f
		            )
				);
		addLines.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
		final NumberAxis rangeAxis = (NumberAxis) addLines.getDomainAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		rangeAxis.setRange(xi, xj);
		ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        JFrame plot = new JFrame("GPR Plot for "+d+"-th dimension");
        plot.setContentPane(chartPanel);
        plot.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        plot.pack();
        RefineryUtilities.centerFrameOnScreen(plot);
        plot.setVisible(true);
	}
	
}
