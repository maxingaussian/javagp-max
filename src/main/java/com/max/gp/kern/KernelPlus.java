package com.max.gp.kern;

import org.jblas.DoubleMatrix;

public class KernelPlus extends Kernel{

	public Kernel k1, k2;
	
	public KernelPlus(Kernel k1_, Kernel k2_) {
		k1=k1_;k2=k2_;
	}

	@Override
	public int getHyperParamsSize() {
		return k1.getHyperParamsSize()+k2.getHyperParamsSize();
	}
	
	@Override
	public void setHyperParams(DoubleMatrix hypers_) {
		k1.setHyperParams(hypers_.getRowRange(0, k1.getHyperParamsSize(), 0));
		k2.setHyperParams(hypers_.getRowRange(k1.getHyperParamsSize(), k1.getHyperParamsSize()+k2.getHyperParamsSize(), 0));
	}

	@Override
	public double compute(DoubleMatrix x1, DoubleMatrix x2) {
		return k1.compute(x1, x2)+k2.compute(x1, x2);
	}

	@Override
	public double computeDerivative(DoubleMatrix x1, DoubleMatrix x2, int d) {
		return k1.computeDerivative(x1, x2, d)+k2.computeDerivative(x1, x2, d-k1.getHyperParamsSize());
	}

}
