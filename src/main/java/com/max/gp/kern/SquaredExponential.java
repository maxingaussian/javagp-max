/**
 * @author Max W. Y. Lam
 */
package com.max.gp.kern;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import com.max.gp.kern.Kernel;

public class SquaredExponential extends Kernel {

	public DoubleMatrix lengthScale;
	
	public SquaredExponential(int dim_, boolean ard_) {
		dim = dim_;
		ard = ard_;
		hyperSize = getHyperParamsSize();
		setHyperParams(DoubleMatrix.rand(hyperSize));
	}

	public int getHyperParamsSize() {
		if(ard){
			return 2*dim+1;
		}
		return 3;
	}

	public void setHyperParams(DoubleMatrix hypers_) {
		super.setHyperParams(hypers_);
		if(ard){
			sigma = hypers.getRowRange(1, dim+1, 0);
			lengthScale = hypers.getRowRange(dim+1, 2*dim+1, 0);
		}
		else{
			sigma = DoubleMatrix.scalar(hypers.get(1));
			lengthScale = DoubleMatrix.scalar(hypers.get(2));
		}
	}

	@Override
	public double compute(DoubleMatrix x1, DoubleMatrix x2) {
		DoubleMatrix dist = MatrixFunctions.pow(x1.sub(x2).div(lengthScale), 2);
		if(ard){
			return Math.exp(-0.5*dist.sum())*MatrixFunctions.pow(sigma,2).prod();
		}
		return sigma.get(0)*sigma.get(0)*
				Math.exp(-0.5*dist.sum()/(lengthScale.get(0)*lengthScale.get(0)));
	}

	@Override
	public double computeDerivative(DoubleMatrix x1, DoubleMatrix x2, int d) {
		double res=0;
		DoubleMatrix dist = MatrixFunctions.pow(x1.sub(x2).div(lengthScale), 2);
		double sum = dist.sum();
		if(ard){
			if(1<=d&&d<dim+1){
				res += 2*MatrixFunctions.pow(sigma,2).prod()/sigma.get(d-1)*Math.exp(-0.5*sum);
			} 
			else if(dim+1<=d&&d<2*dim+1){
				res += MatrixFunctions.pow(sigma,2).prod()*dist.get(d-1-dim)*Math.exp(-0.5*sum)
			            /(lengthScale.get(d-1-dim));
			}
			return res;
		}
		if(d==1){
			res += 2*sigma.get(0)*Math.exp(-0.5*sum);
		} 
		else if(d==2){
			res += sigma.get(0)*sigma.get(0)*sum*Math.exp(-0.5*sum)/(lengthScale.get(0));
		}
		return res;
	}
	
	public static void main(String[] args) {
		SquaredExponential se = new SquaredExponential(1, false);
		se.setHyperParams(new DoubleMatrix(new double[][]{{0.8,0.15}}));
		se.plot(50);
	}
	
}
