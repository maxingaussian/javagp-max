package com.max.gp.kern;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

public class Periodic extends Kernel{

	public DoubleMatrix lengthScale, period;

	public Periodic(int dim_, boolean ard_) {
		dim = dim_;
		ard = ard_;
		hyperSize = getHyperParamsSize();
		setHyperParams(DoubleMatrix.rand(hyperSize));
	}

	public int getHyperParamsSize() {
		if(ard){
			return 3*dim+1;
		}
		return 4;
	}

	public void setHyperParams(DoubleMatrix hypers_) {
		super.setHyperParams(hypers_);
		if(ard){
			sigma = hypers.getRowRange(1, dim+1, 0);
			lengthScale = hypers.getRowRange(dim+1, 2*dim+1, 0);
			period = hypers.getRowRange(2*dim+1, 3*dim+1, 0);
		}
		else{
			sigma = DoubleMatrix.scalar(hypers.get(1));
			lengthScale = DoubleMatrix.scalar(hypers.get(2));
			period = DoubleMatrix.scalar(hypers.get(3));
		}
	}

	public double compute(DoubleMatrix x1, DoubleMatrix x2) {
		if(ard){
			double sigmas = MatrixFunctions.pow(sigma,2).prod();
			double res = sigmas;
			double exp = 0;
			for(int d=0;d<dim;d++){
				double x1d = x1.get(d), x2d = x2.get(d);
				double sin = Math.sin(Math.abs(x1d-x2d)*Math.PI/period.get(d));
				exp += -2/lengthScale.get(d)*sin*sin;
			}
			res*=Math.exp(exp);
			return res;
		}
		double dist = x1.sub(x2).norm1();
		double sin = Math.sin(dist*Math.PI/period.get(0))/lengthScale.get(0);
		sin = sin*sin;
		return sigma.get(0)*sigma.get(0)*Math.exp(-2*sin);
	}

	public double computeDerivative(DoubleMatrix x1, DoubleMatrix x2, int d) {
		if(ard){
			double sigmas = MatrixFunctions.pow(sigma,2).prod();
			double res = sigmas;
			double exp = 0;
			for(int i=0;i<dim;i++){
				double x1d = x1.get(i), x2d = x2.get(i);
				double sin = Math.sin(Math.abs(x1d-x2d)*Math.PI/period.get(i));
				exp += -2/lengthScale.get(i)*sin*sin;
			}
			res*=Math.exp(exp);
			if(1<=d&&d<dim+1){
				res/=sigma.get(d-1)/2;
			}
			else if(dim+1<=d&&d<2*dim+1){
				double x1d = x1.get(d-1-dim), x2d = x2.get(d-1-dim);
				double sin = Math.sin(Math.abs(x1d-x2d)*Math.PI/period.get(d-1-dim));
				res/=lengthScale.get(d-1-dim)/(4*sin*sin);
			}
			else if(2*dim+1<=d&&d<3*dim+1){
				double x1d = x1.get(d-1-2*dim), x2d = x2.get(d-1-2*dim);
				double sin = Math.sin(2*Math.abs(x1d-x2d)*Math.PI/period.get(d-1-2*dim));
				res/=period.get(d-1-2*dim)*period.get(d-1-2*dim)
						*lengthScale.get(d-1-2*dim)*lengthScale.get(d-1-2*dim)/(2*Math.PI*sin*Math.abs(x1d-x2d));
			}
			return res;
		}
		double res=0;
		double dist = x1.sub(x2).norm1();
		double sin = Math.sin(dist*Math.PI/period.get(0))/lengthScale.get(0);
		sin = sin*sin;
		if(d==1){
			res += 2*sigma.get(0)*Math.exp(-2*sin);
		} 
		else if(d==2){
			res += 4*sigma.get(0)*sigma.get(0)*sin*Math.exp(-2*sin)/lengthScale.get(0);
		}
		else if(d==3){
			res += 2*sigma.get(0)*sigma.get(0)*dist*Math.PI*Math.sin(2*dist*Math.PI/period.get(0))*Math.exp(-2*sin)
					/(lengthScale.get(0)*lengthScale.get(0)*period.get(0)*period.get(0));
		}
		return res;
	}

	public static void main(String[] args) {
		Periodic per = new Periodic(1, false);
		per.setHyperParams(new DoubleMatrix(new double[][]{{0.8,0.5,0.3}}));
		per.plot(10);
	}

}
