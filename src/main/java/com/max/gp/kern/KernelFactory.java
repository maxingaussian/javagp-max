package com.max.gp.kern;

public class KernelFactory {

	public static Kernel Plus(Kernel k1, Kernel k2){
		return new KernelPlus(k1, k2);
	}
	
	public static Kernel Prod(Kernel k1, Kernel k2){
		return new KernelProduct(k1, k2);
	}
	
	public static Kernel SEiso(int dim){
		return new SquaredExponential(dim, false);
	}
	
	public static Kernel SEard(int dim){
		return new SquaredExponential(dim, true);
	}
	
	public static Kernel PERiso(int dim){
		return new Periodic(dim, false);
	}
	
	public static Kernel PERard(int dim){
		return new Periodic(dim, true);
	}

}
