/**
 * @author Max W. Y. Lam
 *
 */
package com.max.gp.kern;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

import com.max.gp.plot.PlotTool;

public abstract class Kernel {

	public boolean ard;
	public int hyperSize, dim;
	public double noise;
	public DoubleMatrix hypers, sigma;
	
	public void setNoise(double noise_) {
		noise = noise_;
	}
	
	public double getNoise() {
		return noise;
	}
	
	public abstract int getHyperParamsSize();
	
	public void setHyperParams(DoubleMatrix hypers_) {
		hypers = MatrixFunctions.abs(hypers_);
		setNoise(hypers.get(0));
	}
	
	public DoubleMatrix getHyperParams() {
		return hypers;
	}
	
	public abstract double compute(DoubleMatrix x1, DoubleMatrix x2);

	public abstract double computeDerivative(DoubleMatrix x1, DoubleMatrix x2, int d);
	
	public DoubleMatrix computeMatrix(DoubleMatrix Xn) {
		int n = Xn.rows;
		DoubleMatrix Kn = new DoubleMatrix(n, n);
		for(int i=0;i<n;i++){
			Kn.put(i, i, compute(Xn.getRow(i), Xn.getRow(i))+noise*noise);
			for(int j=i+1;j<n;j++){
				double k_ij = compute(Xn.getRow(i), Xn.getRow(j));
				Kn.put(i, j, k_ij);
				Kn.put(j, i, k_ij);
			}
		}
		return Kn;
	}

	public DoubleMatrix computeMatrix(DoubleMatrix Xm, DoubleMatrix Xn) {
		int m = Xm.rows, n = Xn.rows;
		DoubleMatrix Kmn = new DoubleMatrix(m, n);
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				Kmn.put(i, j, compute(Xm.getRow(i), Xn.getRow(j)));
			}
		}
		return Kmn;
	}
	
	public DoubleMatrix computeDerivativeMatrix(DoubleMatrix Xn, int d) {
		int n = Xn.rows;
		if(d==0){
			return DoubleMatrix.eye(n).mul(2*noise);
		}
		DoubleMatrix Kd = new DoubleMatrix(n, n);
		for(int i=0;i<n;i++){
			for(int j=i;j<n;j++){
				double k_ij = computeDerivative(Xn.getRow(i), Xn.getRow(j), d);
				if(i!=j){
					Kd.put(i, j, k_ij);
					Kd.put(j, i, k_ij);
				}else{
					Kd.put(i, j, k_ij);
				}
			}
		}
		return Kd;
	}
	
	public void plot(int samples) {
		PlotTool.plotKernelwithSamples(this, samples);
	}

}
