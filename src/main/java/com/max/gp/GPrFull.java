/**
 * @author Max W. Y. Lam
 */
package com.max.gp;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Function;

import org.jblas.Decompose;
import org.jblas.DoubleMatrix;
import org.jblas.Geometry;
import org.jblas.MatrixFunctions;
import org.jblas.Solve;

import com.max.gp.alg.Algorithm;
import com.max.gp.kern.Kernel;
import com.max.gp.kern.KernelFactory;
import com.max.gp.opt.Optimizer;
import com.max.gp.opt.OptimizerFactory;
import com.max.gp.plot.PlotTool;

public class GPrFull implements GPr {

	public final static double log2pi = Math.log(2*Math.PI);  
	
	int n;
	Kernel kern;
	DoubleMatrix Xn, Yn, L, LInv, LInvY;
	DoubleMatrix Xmean, Xstd, Xmin, Xmax;
	double Ymean, Ystd;

	public GPrFull(Kernel kern_) {
		Xn = Yn = L = LInv = LInvY = null;
		n = 0;
		kern = kern_;
		Xmean = Xstd = null;
		Ymean = Ystd = 0;
	}

	public boolean isTrained() {
		return LInvY!=null;
	}

	public DoubleMatrix getX(){
		return Xn.mulRowVector(Xstd).addRowVector(Xmean);
	}
	
	public DoubleMatrix getY(){
		return Yn.mul(Ystd).add(Ymean);
	}
	
	public DoubleMatrix getL(){
		return L;
	}

	public double getLikelihood() {
		double logLik = -1*getNegativeLogLikelihood();
		return Math.exp(logLik);
	}

	public double getNegativeLogLikelihood() {
		double Yalpha = Yn.dot(LInv.transpose().mmul(LInvY));
		double logdetK = 2*Math.log(L.diag().prod());
		double constant = n*log2pi;
		return (Yalpha+logdetK+constant)*1./2;
	}
	
	public DoubleMatrix getNegativeLogLikelihoodGradient() {
		DoubleMatrix alpha = LInv.transpose().mmul(LInvY);
		DoubleMatrix tmp = alpha.mmul(alpha.transpose()).sub(LInv.transpose().mmul(LInv));
		DoubleMatrix grad = new DoubleMatrix(getParamsSize(), 1);
		for(int d=0;d<getParamsSize();d++){
			grad.put(d, 0, -0.5*Algorithm.computeTrace(tmp, kern.computeDerivativeMatrix(Xn, d), 0.01));
		}
		return grad;
	}
	
	public int getParamsSize() {
		return kern.getHyperParamsSize(); // output noise
	}
	
	public void setKernel(Kernel kern_) {
		kern = kern_;
	}
	
	public Kernel getKernel() {
		return kern;
	}
	
	public void setParams(DoubleMatrix params_) {
		kern.setHyperParams(params_);
		DoubleMatrix Kn = kern.computeMatrix(Xn);
		L = Decompose.cholesky(Kn).transpose();
		LInv = Solve.solve(L, DoubleMatrix.eye(n));
		LInvY = LInv.mmul(Yn);
	}
	
	public DoubleMatrix getParams() {
		return kern.getHyperParams();
	}

	public void train(DoubleMatrix Xm_, DoubleMatrix Ym_) {
		int m = Xm_.rows;
		n=m;
		Xn = Xm_;
		Yn = Ym_;
		Xmin = Xn.columnMins();
		Xmax = Xn.columnMaxs();
		Xmean = Xn.columnMeans(); 
		Ymean = Yn.mean();
		Xstd = MatrixFunctions.sqrt(MatrixFunctions.pow(Xn.subRowVector(Xmean),2).columnMeans());
		Ystd = Yn.norm2()/Math.sqrt(n);
		Xn = (Xn.subRowVector(Xmean)).divRowVector(Xstd);
		Yn = (Yn.sub(Ymean)).div(Ystd);
		DoubleMatrix Kn = kern.computeMatrix(Xn);
		L = Decompose.cholesky(Kn).transpose();
		LInv = Solve.solve(L, DoubleMatrix.eye(n));
		LInvY = LInv.mmul(Yn);
	}

	public void optimize(Optimizer opt, int samples, int iterations, boolean message) {
		opt.init(getParamsSize(), 
		new Function<DoubleMatrix, Void>() {
			public Void apply(DoubleMatrix params) {
				setParams(params);
				return null;
			}
		},
		new Function<Void, Double>() {
			public Double apply(Void t) {
				double nll = getNegativeLogLikelihood();
				if(Double.isFinite(nll))
					return nll;
				return 1E10;
			}
		},
		new Function<Void, DoubleMatrix>() {
			public DoubleMatrix apply(Void t) {
				return getNegativeLogLikelihoodGradient();
			}
		});
		opt.run(samples, iterations, message);
	}

	public ArrayList<DoubleMatrix> predict(DoubleMatrix Xs, boolean var, boolean prob) {
		Xs = (Xs.subRowVector(Xmean)).divRowVector(Xstd);
		DoubleMatrix Ksn = kern.computeMatrix(Xs, Xn);
		ArrayList<DoubleMatrix> res = new ArrayList<DoubleMatrix>();
		res.add(Ksn.mmul(LInv.transpose().mmul(LInvY)).mul(Ystd).add(Ymean));
		if(var){
			DoubleMatrix Ks = kern.computeMatrix(Xs);
			DoubleMatrix tmp = LInv.mmul(Ksn.transpose());
			res.add(Ks.sub(tmp.transpose().mmul(tmp)));
		}
		return res;
	}
	
	public void plotData(int d) {
		PlotTool.plotGPwithData(this, d, Xmin.get(d)-1, Xmax.get(d)+1);
	}
	
}
